#include "main.h"

int main(int argc, char *argv[]){

    FILE *fp;

    int c;
    int fromFormat = 42;
    int toFormat = 33;

    if(argc < 3){
        errno = EINVAL;
        perror("Not enough input arguments\nThis program requires a drill file and format\nIf the drill format is 3:3 enter 33");
        return -1;
    }
    if(argc == 3){
        // printf("The file supplied is '%s'\n", argv[1]);

        // printf("The drill format is '%s'\n", argv[2]);
    }
    if (argc > 3){
        
    }
 
    fp = fopen(argv[1], "r");

    if(fp == NULL){
        perror("Error opening file");
        return -1;
    }

    if(!sscanf(argv[2], "%u", &fromFormat)){
        errno = EINVAL;
        perror("Invalid format");
        return -1;
    }


    c = fgetc(fp);
    while(1) {
        
        if( feof(fp) ) { 
            break;
        }

        printf("%c", c);

        if (c == 'X' || c == 'Y') {

            char str[16] = {};

            // load numbers into string until either Y or \n
            for (size_t i = 0; i < 12; i++){
                c = fgetc(fp);

                if (strchr("+-0123456789", c) == NULL) {
                    break;
                }

                *(str + i) = c;

                
            }
            
            format(str, fromFormat, toFormat, LZ);        
            printf("%s", str);
        }

        else {
            c = fgetc(fp);
        }
   }
    
    fclose(fp);

    // printf("\n\n");

    return 0;
}



int format ( char * str, int from, int to, int zs){
    int sign = 0;

    int integerFrom = from / 10;
    int decimalFrom = from % 10;
    int integerTo = to / 10;
    int decimalTo = to % 10;

    char integer[16] = "";
    char decimal[16] = "";

    if(strpbrk (str, "+-") != NULL){
        sign = 1;
    }

    if (zs == LZ){
        // fill start with zero
        padLeft(str, integerFrom + decimalFrom);
    }
    else if (zs == TZ){
        // fill end with zero
        padRight(str, integerFrom + decimalFrom);
    }

    strncpy(integer, str, (integerFrom + sign));
    strncpy(decimal, (str + integerFrom + sign), decimalFrom);

    if(integerTo >= integerFrom){
        // pad out
        padLeft(integer, integerTo);
    }
    else {
        for (size_t i = 0; *(integer + sign + i) != '\0' ; i++) {
            *(integer + sign + i) = *(integer + sign + i + (integerFrom-integerTo));
        }
              
    }

    if(decimalTo >= decimalFrom){
        // pad out 
        padRight(decimal, decimalTo);
    }
    else {
        *(decimal + decimalTo) = '\0';
    }



    // printf("%s\n", integer);
    // printf("%s\n", decimal);

    strcpy(str, integer);
    strcat(str, decimal);

    suppressZeros(str, zs);

    // printf("%s\n", str);


    return 0;
};

int padLeft (char * str, int len){
    char pad[16] = "";

    if(strpbrk (str, "+-") != NULL){
        len++;
    }

    strncat(pad, "000000000000000", len-strlen(str));
    strcat(pad, str);

    // move sign to front

    char * sign = strpbrk (pad, "+-");

    if(sign != NULL){
        char s = *sign;
        *sign = '0';
        *pad = s;
    }

    strcpy(str, pad);

    return 0;
}

int padRight (char * str, int len){
    char pad[16] = "";

    if(strpbrk (str, "+-") != NULL){
        len++;
    }

    strncat(pad, "000000000000000", len-strlen(str));
    strcat(str, pad);

    // move sign to front

    // char * sign = strpbrk (str, "+-");

    // if(sign != NULL){
    //     char s = *sign;
    //     *sign = '0';
    //     *str = s;
    // }

    return 0;
}

int suppressZeros (char * str, int zs){

    char new[16] = "";
    char c;
    int done = 0;
    int j = 0;



    for (int i = 0; i < strlen(str); i++){
        c = *(str + i);
        if(c == '+' || c == '-'){
            *(new + j) = c;
            j++;
        }
        else if (c != '0'){
            strcpy(new+j, (str+i));
            break;
        }


    }

    strcpy(str, new);

    return 0;
}
