CC=gcc
CFLAGS= -Wall -fsanitize=undefined -O

main: main.c
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)

.PHONY: clean

clean:
	rm -f *.o *~ main
