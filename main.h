#include <stdio.h>
#include <string.h>
#include <errno.h>

enum zeroSuppression {
    NONE,
    LZ,
    TZ
};


int format (char *, int, int, int);

int padLeft (char *, int);

int padRight (char *, int);

int suppressZeros (char *, int);